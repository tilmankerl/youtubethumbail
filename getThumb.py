import urllib.request
import socket
import webbrowser

__author__ = "MisterXY89"
__version__ = "2015.11.9"


#############################################
#       get YouTube thumbnail images        #
#     tested with 10 YT-urls -> worked      #
#############################################


def getUserIp(): #is not necessaryily needed
    """ get user ip """    
    return socket.gethostbyname_ex(socket.gethostname())[2][1]


def openUrl(url):
    """ opens url, reads content and returns it """
    try:        
        openUrl = urllib.request.urlopen(url)
        data = openUrl.read()
        stringData = data.decode(encoding = "ISO-8859-1")        
        return stringData        
    except:
        print("URL could not be found.")
        return False



def searchThumbnailUrl(url):
    """ searching thumbnailUrl in given data and return link """
    dataList = []
    link = ""
    keyword = "thumbnailUrl"
    ending = "maxresdefault.jpg"
    ending2 ="hqdefault.jpg"
    data = openUrl(url)
    if not data:
        return False
    try:
        for x in range(0, data.count(ending)-2):        
            i = data.index(ending)
            data = data[i+len(ending):]
    except:
        for x in range(0, data.count(ending2)-2):        
            i = data.index(ending2)
            data = data[i+len(ending2):]
    indexT = data.index(keyword)    
    start = indexT + 20
    try:
        ende = data.index(ending)+len(ending)
    except:
        ende = data.index(ending2)+len(ending2)
    laenge = ende - start    
    for x in range(0, laenge):
        link += data[start+x]
    if len(link) > 10:
        # webbrowser.open(link)
        return link    
    return 'False'

                        
def top():
    while True:
        #complete url needed (eg. https://www.youtube.com/watch?v=OYqZbA26m60
        # -> just copy it from your browser
        url = input("Please enter YouTube URL:\n-> ")
        print("Link: " + searchThumbnailUrl(url))
        
        a = input("Hit Return to restart or 'q' and then Return to quit.")
        if a =="q":
            return False
        else:
            continue
