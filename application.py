

from flask import Flask, render_template, request, send_from_directory
from getThumb import searchThumbnailUrl

app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
	return render_template("base.html", title="Home")

@app.route("/thumbnail")
def thumbnail():
	return render_template("geturl.html", title="Get Thumbnail URL")

@app.route("/get_thumbnail", methods=['POST'])
def get_thumbnail():
	yt_url = request.form['yt_url']
	img_url = searchThumbnailUrl(yt_url)
	return render_template("thumbnail.html", url=img_url, title="Your thumbnail is ready")

@app.route("/code")
def code():
	return render_template("code.html", title="The code")

if __name__ == "__main__":
    app.run(host='0.0.0.0																																', port=5000)#debug=True)


 #https://www.youtube.com/watch?v=Ju86mknumYM